<?php
/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 07.12.14
 * Time: 17:51
 */

/** send all parsed data to reciever */
class ReceiverConnector
{

    public $error;

    /** send data to receiver site
     * @param string $data
     * @param string $url
     * @return bool
     */
    public function sendData($data, $url)
    {
        $result = $this->postJson($data, $url);
        return $result;
    }

    /** posing json string to url
     *
     * @param string $jsonString
     * @param string $url
     */
    protected function postJson($jsonString, $url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonString))
        );

        $result = curl_exec($ch);
        if ($result === false) {
            $this->error = curl_error($ch);
            Logger::log('error co connect to Reciever  : ' . json_encode($this->error),'error');
            return false;
        }
        return true;
    }
} 