<?php

/**
 * Created by PhpStorm.
 * User: lldl
 * Date: 17.12.14
 * Time: 23:26
 */

class TournamentsParser extends AbstractParser
{
    public $loginUrl = 'http://egamingbets.com/?mod=secure_login';
    public $cookies = "s1=3fd21760709abb9d0950eacc0d28dfab93e4b559;s2=6015e16538939c97a8afdd1692c9f8bcbf75b87e;__cfduid=d2e1e4f645c75fbbc6b349308ac8145701418847143; cf_clearance=186e883ab78ff4fbc82ce45c35cf919c06df0af5-1418847203-1800; gryphon=uluaistj5kpl21c70v2o4rlt15; is_first_time=1; referer=http://egamingbets.com/; lang=en; sound_enabled=1";

    public $tournamentsUrl = "http://egamingbets.com/ajax.php";

    /** login before parse */
    public function login()
    {
        $url = $this->loginUrl;
        $postfields = array(
            '_csrf_token' => '662937e10a22772e215ec5717426e000',
            'loginuser' => '1',
            'email' => 'll25',
            'password' => 'satory_999'
        );
        $curlopts = array(
            CURLOPT_COOKIESESSION => TRUE,
            CURLOPT_COOKIE => $this->cookies,
        );
        $response = $this->makePostRequest($url, $postfields, $curlopts);

        return $response;
    }

    /** parse tournaments */
    public function parse()
    {
        $params = array(
            'key' => 'modules_home_view_getvotinglist',
            'type' => 'modules',
            'ind' => 'home',
            'ajax' => 'view',
            'act' => 'getvotinglist',
        );

        $curlopts = array(
            CURLOPT_COOKIESESSION => TRUE,
            CURLOPT_COOKIE => $this->cookies,
        );

        $response = $this->getResponse($this->tournamentsUrl, $params, $curlopts);
        return $response;
    }

}